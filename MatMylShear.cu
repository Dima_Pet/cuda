﻿
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "time.h"
#include <stdio.h>
#include <fstream>

#define BLOCK_SIZE 32


__global__ void MultMatrix_ker(float* c, float* a, float* b, int n)
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    int j = blockIdx.y * blockDim.y + threadIdx.y;
    float sum = 0;
    if ((i < n) && (j < n))
    {
        for (int k = 0; k < n; k++)
        {
            sum += a[i * n + k] + b[k * n + j];
        }
        c[i * n + j] = sum;
    }
}
__global__ void MultMatrixShear_ker(float* c, float* a, float* b, int n)
{
    int bx = blockIdx.x, by = blockIdx.y;
    int tx = threadIdx.x, ty = threadIdx.y;

    int aBegin = n * BLOCK_SIZE * by, aEnd = aBegin + n - 1;
    int bBegin = BLOCK_SIZE * bx, aStep = BLOCK_SIZE, bStep = BLOCK_SIZE * n;

    float sum1 = 0.0f;
    __shared__ float as[BLOCK_SIZE][BLOCK_SIZE];
    __shared__ float bs[BLOCK_SIZE][BLOCK_SIZE];

    for (int ia = aBegin, ib = bBegin; ia <= aEnd; ia += aStep, ib += bStep)
    {
        as[ty][tx]=a[ia+n*ty+tx];
        bs[ty][tx]=b[ib+n*ty+tx];
        __syncthreads();
        for (int k=0;k<BLOCK_SIZE;k++)
        {
            sum1+=as[ty][k]*bs[k][tx];      
        }
        __syncthreads();
    }
    c[aBegin+bBegin+ty*n+tx] = sum1;
}

void MultMatrixShear(int n)
{
    float* hA, * hB, * hC, * dA, * dB, * dC;
    float timerValueGPU_Shear, timerValueGPU;
    cudaError_t cudaStatus;
    cudaEvent_t start, stop;
    cudaEventCreate(&start); cudaEventCreate(&stop);
    hA = (float*)malloc(n*n*sizeof(float)); 
    hB = (float*)malloc(n * n * sizeof(float)); 
    hC = (float*)malloc(n * n * sizeof(float)); 
   
    cudaMalloc((void**)&dA, n * n * sizeof(float));
    cudaMalloc((void**)&dB, n * n * sizeof(float));
    cudaMalloc((void**)&dC, n * n * sizeof(float));
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            hA[i * n + j] = 1 + i + j;
            hB[i * n + j] = 1 + (i + j) * (i + j);
            hC[i * n + j] = 0;

        }
    }
    dim3 threads(BLOCK_SIZE, BLOCK_SIZE);
    dim3 blocks(n / threads.x, n / threads.y);

    clock_t START = clock();
    cudaMemcpy(dA, hA, n * n * sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(dB, hB, n * n * sizeof(float), cudaMemcpyHostToDevice);
    MultMatrixShear_ker <<<blocks, threads>> > (dC, dA, dB, n);
    cudaMemcpy(hC, dC, n * n * sizeof(float), cudaMemcpyDeviceToHost);
    timerValueGPU_Shear = (float)(clock() - START) / CLOCKS_PER_SEC;
    printf("GPU calculation time with Shear %f s\n", timerValueGPU_Shear);
    free(hA);
    free(hB);
    free(hC);
    cudaFree(dA);
    cudaFree(dB);
    cudaFree(dC);
}
void MultMatrix(int n, char ch)
{
    float* hA, * hB, * hC, * dA, * dB, * dC;
    float timerValueGPU, timerValueCPU;
    cudaError_t cudaStatus;
    hA = (float*)malloc(n * n * sizeof(float));
    hB = (float*)malloc(n * n * sizeof(float));
    hC = (float*)malloc(n * n * sizeof(float));

    cudaMalloc((void**)&dA, n * n * sizeof(int));
    cudaMalloc((void**)&dB, n * n * sizeof(int));
    cudaMalloc((void**)&dC, n * n * sizeof(int));
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            hA[i * n + j] = 1 + i + j;
            hB[i * n + j] = 1 + (i + j) * (i + j);
            hC[i * n + j] = 0;
        }
    }

    clock_t START = clock();
    cudaMemcpy(dA, hA, n * n * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(dB, hB, n * n * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(dC, hC, n * n * sizeof(int), cudaMemcpyHostToDevice);
    dim3 threads_1(BLOCK_SIZE, BLOCK_SIZE);
    dim3 blocks_1(n / threads_1.x, n / threads_1.y);
    MultMatrix_ker <<<blocks_1, threads_1 >>> (dC, dA, dB, n);
    cudaMemcpy(hC, dC, n * n * sizeof(int), cudaMemcpyDeviceToHost);
    timerValueGPU = (float)(clock() - START) / CLOCKS_PER_SEC;
    printf("GPU calculation time without Shear: %f s\n", timerValueGPU);

    //CPU вариант 
    if (ch == 'y')
    {
        printf("Start CPU calculation\n");
        clock_t START_CPU = clock();

        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n; j++)
            {
                int sum = 0;
                for (int k = 0; k < n; k++)
                {
                    sum += hA[i * n + k] * hB[k + n * j];
                }
                hC[i * n + j] = sum;
            }
        }
        timerValueCPU = (float)(clock() - START_CPU) / CLOCKS_PER_SEC;
        printf("CPU calculation time: %f s\n", timerValueCPU);
        // Вывод коэффициента ускорения
        printf("Rate: %fx\n", timerValueCPU / timerValueGPU);
    }
   
Error:
    cudaFreeHost(hA);
    cudaFreeHost(hB);
    cudaFreeHost(hC);
    cudaFree(dA);
    cudaFree(dB);
    cudaFree(dC);
}
int main()
{
    int n = 2048*8;
    char ch;
    printf("With CPU?(y/n)\n");
    ch=getchar();
    MultMatrixShear(n);
    MultMatrix(n,ch);

    return 0;
}
