﻿
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#ifdef __CUDACC__
#define cuda_SYNCTHREADS() __syncthreads()
#else
#define cuda_SYNCTHREADS()
#endif
#include "time.h"
#include <stdio.h>
#include <fstream>

__global__ void addVector_ker(int *c, int *a,int *b,int n)
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if (i < n)
    {
        c[i] = a[i] + b[i];
    }
}
__global__ void addMatrix_ker(int* c, int* a, int* b, int n)
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    int j = blockIdx.y * blockDim.y + threadIdx.y;
    if ((i < n)&&(j<n))
    {
        c[i*n+j] = a[i*n+j] + b[i*n+j];
    }
}
__global__ void MultMatrix_ker(int* c, int* a, int* b, int n)
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    int j = blockIdx.y * blockDim.y + threadIdx.y;
    float sum = 0;
    if ((i < n) && (j < n))
    {
        for (int k = 0; k < n; k++)
        {
            sum += a[i * n + k] + b[k * n + j];
        }
        c[i * n + j] = sum;
    }
}
void AddVector( int n, int nThread , int nBlocks)
{
    int* hA, * hB, * hC, * dA, * dB, * dC;
    float timerValueGPU, timerValueCPU;
    cudaError_t cudaStatus;
    cudaStatus = cudaHostAlloc((void**)&hA, n * sizeof(int), cudaHostAllocDefault);
    if (cudaStatus != cudaSuccess)
    {
        fprintf(stderr, "Failed host Alloc hA");
        goto Error;
    }
    cudaStatus = cudaHostAlloc((void**)&hB, n * sizeof(int), cudaHostAllocDefault);
    if (cudaStatus != cudaSuccess)
    {
        fprintf(stderr, "Failed host Alloc hB");
        goto Error;
    }
    cudaStatus = cudaHostAlloc((void**)&hC, n * sizeof(int), cudaHostAllocDefault);
    if (cudaStatus != cudaSuccess)
    {
        fprintf(stderr, "Failed host Alloc hC");
        goto Error;
    }

    cudaMalloc((void**)&dA, n * sizeof(int));
    cudaMalloc((void**)&dB, n * sizeof(int));
    cudaMalloc((void**)&dC, n * sizeof(int));
    for (int i = 0; i < n; i++)
    {
        hA[i] = 1 + i;
        hB[i] = 1 + i * i;
        hC[i] = 0;
    }

    clock_t START = clock();


    cudaMemcpy(dA, hA, n * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(dB, hB, n * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(dC, hC, n * sizeof(int), cudaMemcpyHostToDevice);
    addVector_ker << <nBlocks, nThread >> > (dC, dA, dB, n);

    cudaMemcpy(hC, dC, n * sizeof(int), cudaMemcpyDeviceToHost);

    timerValueGPU = (float)(clock() - START) / CLOCKS_PER_SEC;
    printf("GPU calculation time: %f s\n", timerValueGPU);

    //CPU вариант 
    clock_t START_CPU = clock();
    for (int i = 0; i < n; i++)
    {
        hC[i] = hA[i] + hB[i];
    }
    timerValueCPU = (float)(clock() - START_CPU) / CLOCKS_PER_SEC;
    printf("CPU calculation time: %f s\n", timerValueCPU);
    // Вывод коэффициента ускорения
    printf("Rate: %fx\n", timerValueCPU / timerValueGPU);

Error:
    cudaFreeHost(hA);
    cudaFreeHost(hB);
    cudaFreeHost(hC);
    cudaFree(dA);
    cudaFree(dB);
    cudaFree(dC);

}
void AddMatrix(int n, int nThread, int nBlocks)
{
    int* hA, * hB, * hC, * dA, * dB, * dC;
    float timerValueGPU, timerValueCPU;
    cudaError_t cudaStatus;
    cudaStatus = cudaHostAlloc((void**)&hA, n * n * sizeof(int), cudaHostAllocDefault);
    if (cudaStatus != cudaSuccess)
    {
        fprintf(stderr, "Failed host Alloc hA");
        goto Error;
    }
    cudaStatus = cudaHostAlloc((void**)&hB, n * n * sizeof(int), cudaHostAllocDefault);
    if (cudaStatus != cudaSuccess)
    {
        fprintf(stderr, "Failed host Alloc hB");
        goto Error;
    }
    cudaStatus = cudaHostAlloc((void**)&hC, n * n * sizeof(int), cudaHostAllocDefault);
    if (cudaStatus != cudaSuccess)
    {
        fprintf(stderr, "Failed host Alloc hC");
        goto Error;
    }

    cudaMalloc((void**)&dA, n * n * sizeof(int));
    cudaMalloc((void**)&dB, n * n * sizeof(int));
    cudaMalloc((void**)&dC, n * n * sizeof(int));
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            hA[i * n + j] = 1 + i + j;
            hB[i * n + j] = 1 + (i + j) * (i + j);
            hC[i * n + j] = 0;
            //printf("%f+%f ", hA[i * n + j], hB[i * n + j]);
        }
        //   printf("\n");
    }

    clock_t START = clock();

    cudaMemcpy(dA, hA, n * n * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(dB, hB, n * n * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(dC, hC, n * n * sizeof(int), cudaMemcpyHostToDevice);

    dim3 threads(nBlocks, nBlocks);
    dim3 blocks(n / threads.x, n / threads.y);

    addMatrix_ker <<<blocks, threads >> > (dC, dA, dB, n);

    cudaMemcpy(hC, dC, n * n * sizeof(int), cudaMemcpyDeviceToHost);

    timerValueGPU = (float)(clock() - START) / CLOCKS_PER_SEC;
    printf("GPU calculation time: %f s\n", timerValueGPU);

    //CPU вариант 
    clock_t START_CPU = clock();

    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            hC[i * n + j] = hA[i * n + j] + hB[i * n + j];
        }
    }
    timerValueCPU = (float)(clock() - START_CPU) / CLOCKS_PER_SEC;
    printf("CPU calculation time: %f s\n", timerValueCPU);
    // Вывод коэффициента ускорения
    printf("Rate: %fx\n", timerValueCPU / timerValueGPU);
    Error:
    cudaFreeHost(hA);
    cudaFreeHost(hB);
    cudaFreeHost(hC);
    cudaFree(dA);
    cudaFree(dB);
    cudaFree(dC);
}
void MultMatrix(int n, int nThread, int nBlocks)
{
    int* hA, * hB, * hC, * dA, * dB, * dC;
    float timerValueGPU, timerValueCPU;
    cudaError_t cudaStatus;
    cudaStatus = cudaHostAlloc((void**)&hA, n * n * sizeof(int), cudaHostAllocDefault);
    if (cudaStatus != cudaSuccess)
    {
        fprintf(stderr, "Failed host Alloc hA");
        goto Error;
    }
    cudaStatus = cudaHostAlloc((void**)&hB, n * n * sizeof(int), cudaHostAllocDefault);
    if (cudaStatus != cudaSuccess)
    {
        fprintf(stderr, "Failed host Alloc hB");
        goto Error;
    }
    cudaStatus = cudaHostAlloc((void**)&hC, n * n * sizeof(int), cudaHostAllocDefault);
    if (cudaStatus != cudaSuccess)
    {
        fprintf(stderr, "Failed host Alloc hC");
        goto Error;
    }

    cudaMalloc((void**)&dA, n * n * sizeof(int));
    cudaMalloc((void**)&dB, n * n * sizeof(int));
    cudaMalloc((void**)&dC, n * n * sizeof(int));
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            hA[i * n + j] = 1 + i + j;
            hB[i * n + j] = 1 + (i + j) * (i + j);
            hC[i * n + j] = 0;

        }
    }

    clock_t START = clock();

    cudaMemcpy(dA, hA, n * n * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(dB, hB, n * n * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(dC, hC, n * n * sizeof(int), cudaMemcpyHostToDevice);

    dim3 threads(nBlocks, nBlocks);
    dim3 blocks(n / threads.x, n / threads.y);

    MultMatrix_ker<<<blocks, threads >> > (dC, dA, dB, n);

    cudaMemcpy(hC, dC, n * n * sizeof(int), cudaMemcpyDeviceToHost);


    timerValueGPU = (float)(clock() - START) / CLOCKS_PER_SEC;
    printf("GPU calculation time: %f s\n", timerValueGPU);

    //CPU вариант 
    clock_t START_CPU = clock();

    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            int sum = 0;
            for (int k = 0; k < n; k++)
            {
                sum += hA[i * n + k] * hB[k + n * j];
            }
            hC[i * n + j] = sum;
        }
    }
    timerValueCPU = (float)(clock() - START_CPU) / CLOCKS_PER_SEC;
    printf("CPU calculation time: %f s\n", timerValueCPU);
    // Вывод коэффициента ускорения
    printf("Rate: %fx\n", timerValueCPU / timerValueGPU);
    Error:
    cudaFreeHost(hA);
    cudaFreeHost(hB);
    cudaFreeHost(hC);
    cudaFree(dA);
    cudaFree(dB);
    cudaFree(dC);
}
int main()
{
    int n=512;
    int nThread = 8;
    int nBlocks;
    int choose = 0;
    printf("Enter number of elements N (size=%d*N)\n",nThread);
    scanf("%d",&n);
    n *= nThread;
    if ((n % nThread) == 0)
    {
        nBlocks = n / nThread;
    }
    else
    {
        nBlocks = (int)(n / nThread) + 1;
    }
    while (1)
    {
        printf("Choose operation:\n0-Vector addition\n1-Matrix addition\n2-Matrix multipliction\n");
        scanf("%d", &choose);
        if (choose == 0)
        {
            AddVector(n, nThread, nBlocks);
        }
        if (choose == 1)
        {
            AddMatrix(n, nThread, nBlocks);
        }
        if (choose == 2)
        {
            MultMatrix(n, nThread, nBlocks);
        }
    }
   
    return 0;
}
